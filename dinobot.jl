using Xdo, NVFBC, Images

mutable struct DinoBot
    obstaclerows::Array{Int}
    dinoendcol::Int
    lastpos::Int
    moving_avg_speed::Float64
    obstaclewidth::Float64
    lastimage::Matrix{Gray}
end

prepare_image() = return Gray.(NVFBC.cuda_frame_to_image(grab_frame()[:,1:16:end,200:16:end-200]))

bot = DinoBot([26, 30], 20, -1, 80., 0., prepare_image())

function should_jump(bot::DinoBot, frametime)
    image = prepare_image()
    lastpos = bot.lastpos
    obstwidth = 0
    obstpos = -1
    for row in eachrow(image[bot.obstaclerows,:])
        width = 0
        pos = -1
        lastcol = row[bot.dinoendcol]
        obstcol = lastcol
        for i in bot.dinoendcol+1:size(image,2)
            currcol = row[i]
            if abs(lastcol-currcol) > 0.1 && width == 0
                if obstpos >= 0 && i > obstpos
                    println("$obstpos, $i")
                    break
                end
                pos = i
                obstcol = currcol
            end
            if pos >= 0
                if currcol != obstcol && currcol == lastcol
                    obstwidth = width
                    obstpos = pos
                    break
                end
                width += 1
            end
            lastcol = currcol
        end
    end
    obstpos == -1 && return false, 0
    bot.lastpos = obstpos
    lastpos == -1 && return false, 0
    change = lastpos - obstpos
    if change > 0
        bot.moving_avg_speed = 0.99 * bot.moving_avg_speed + 0.01 * change / frametime
    end
    if obstwidth <= 16
        bot.obstaclewidth = max(obstwidth, bot.obstaclewidth)
    end
    eta = (obstpos - bot.dinoendcol + 5) / bot.moving_avg_speed

    println("eta=$eta, speed=$(bot.moving_avg_speed), change=$change, width=$obstwidth, realwidth=$(bot.obstaclewidth), obstpos=$obstpos")
    jump = 0 < eta < 0.18
    duration = 0.13 + 5 / bot.moving_avg_speed * (log2(max(1, bot.obstaclewidth)) + 1)
    if jump
        bot.lastpos = -1
        bot.obstaclewidth = 0
    end
    return jump, duration
end

function run!(bot, frame_time = 0.016)
    next_frame_time = frame_time
    while true
        t0 = time_ns()
        jump, duration = should_jump(bot, next_frame_time)
        if jump
            keys_up("Down")
            keys_down("space")
            sleep(duration)
            keys_up("space")
            keys_down("Down")
        else
            sleep(frame_time)
        end
        next_frame_time = (time_ns() - t0) / 1e9
    end    
end